# gaivota_spider

A spider used to fetch climate of given city

## Installation

1. Requirements
    - git
    - Python3.6
    - Docker
    - docker-compose
    
2. Clone the project to local place

    `https://gitlab.com/liang.guisheng/gaivota_spider.git`
    
3. Run spider service with scheduler
    ```shell
    cd gaivota_spider
    docker-compose up
    ```
   
4. You will see the spider will search the climate information of Taubaté once an hour



