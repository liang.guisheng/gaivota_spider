#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/11/20 1:16 PM
@Author: liang
@File: main.py
"""
import os
import subprocess
from datetime import datetime

from apscheduler.schedulers.blocking import BlockingScheduler

from settings import SPIDER_PROJECT_PATH, SCRAPY_SETTINGS_MODULE, PROJECT_BASE_DIR


def run_spider(spider_name: str):
    """ spider runner factory for scheduler """
    def wrapped_spider():
        print(f'Starting spider {spider_name} at {datetime.now()}')
        os.environ.update({
            'PYTHONPATH': SPIDER_PROJECT_PATH,
            'SCRAPY_SETTINGS_MODULE': SCRAPY_SETTINGS_MODULE
        })
        subprocess.run(
            f'scrapy crawl --loglevel WARNING {spider_name}'.split())
        os.environ.update({'PYTHONPATH': PROJECT_BASE_DIR})

    return wrapped_spider


scheduler = BlockingScheduler()
# run this command once an hour
climate_spider_job = scheduler.add_job(run_spider('climate'),
                                       'interval',
                                       hours=1)

if __name__ == '__main__':
    print(f'Starting scheduler at {datetime.now()}')
    scheduler.start()
