# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class GaivotaSpidersItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    location = scrapy.Field()
    wind = scrapy.Field()
    icon = scrapy.Field()
    current_temperature = scrapy.Field()
    sensation_temperature = scrapy.Field()
    moisture = scrapy.Field()
    update_time = scrapy.Field(serializer=str)
