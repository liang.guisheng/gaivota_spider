#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/11/20 9:42 AM
@Author: liang
@File: climates.py
"""
import re
from datetime import datetime
from typing import Dict, Optional

import pytz
import scrapy
from scrapy import Selector

from ..items import GaivotaSpidersItem

BASE_SITE_URL = 'https://www.climatempo.com.br'


def clean_non_digit_suffix(value: str):
    value = value.strip('\n ')
    last_digit_index = 0
    for i, c in enumerate(value):
        if not c.isdigit():
            break
        last_digit_index = i
    return value[0:last_digit_index + 1]


class ClimateSpider(scrapy.Spider):
    """ Spider to fetch climate data """

    name = 'climate'
    start_urls = [
        'https://www.climatempo.com.br/previsao-do-tempo/agora/cidade/566/taubate-sp'
    ]

    def parse(self, response, **kwargs) -> Dict:
        location_item = response.css('div.base-card.-no-bottom._margin-b-2')
        climate_items = response.css(
            'div.base-card.-no-top.-no-bottom>div.row.no-gutters')
        update_item = response.css(
            'div.base-card.-no-top._border-t-gray-light')
        data = {
            'location':
            self._parse_location(location_item),
            'icon':
            self._parse_climate_icon(climate_items[0]),
            'current_temperature':
            self._parse_current_temperature(climate_items[0]),
            'sensation_temperature':
            self._parse_sensation_temperature(climate_items[0]),
            'wind':
            self._parse_wind(climate_items[1]),
            'moisture':
            self._parse_moisture(climate_items[1]),
            'update_time':
            self._parse_update_time(update_item)
        }

        item = GaivotaSpidersItem(**data)

        yield item

    @staticmethod
    def _parse_location(item: Selector) -> Dict:
        location_tuple = item.css('p.-gray span.-bold.-dark-blue::text').get(
        ).strip('\n ').split('-')
        return {
            'city': location_tuple[0],
            'state': location_tuple[1],
            'country': 'BR'
        }

    @staticmethod
    def _parse_climate_icon(item: Selector) -> str:
        return f'{BASE_SITE_URL}{item.css("img._height-80::attr(src)").get()}'

    @staticmethod
    def _parse_current_temperature(item: Selector) -> float:
        return float(
            clean_non_digit_suffix(item.css('span.-helvetica::text').get()))

    @staticmethod
    def _parse_sensation_temperature(item: Selector) -> float:
        return float(
            clean_non_digit_suffix(
                item.css('span.-helvetica p span::text').get()))

    @staticmethod
    def _parse_wind(item: Selector) -> Dict:
        return {
            'speed':
            float(
                clean_non_digit_suffix(
                    item.css('ul.list')[2].css('li._margin-b-15')[0].css(
                        'p.-gray::text').get())),
            'type':
            item.css('ul.list')[1].css('li._margin-b-15')[0].css(
                'p.-gray::text').getall()[1].strip('\n ')
        }

    @staticmethod
    def _parse_moisture(item: Selector) -> str:
        return item.css('ul.list')[1].css('li')[1].css(
            'p.-gray::text').get().strip('\n ')

    @staticmethod
    def _parse_update_time(item: Selector) -> Optional[datetime]:
        match = re.search(r'\b(\d{1,2}):(\d{1,2})\b',
                          item.css('p::text').get())
        if not match:
            return None
        hour, minute = match.groups()
        updated_time = datetime.now().replace(
            hour=int(hour),
            minute=int(minute),
            second=0,
            microsecond=0,
            tzinfo=pytz.timezone('America/Sao_Paulo'))
        return updated_time
