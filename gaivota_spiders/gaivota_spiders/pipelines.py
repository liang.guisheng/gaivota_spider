# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

# useful for handling different item types with a single interface
from pymongo import MongoClient
from itemadapter import ItemAdapter


class GaivotaSpidersPipeline:

    collection_name = 'climates'

    def __init__(self, mongo_uri: str = None, mongo_db: str = None):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(mongo_uri=crawler.settings.get('MONGO_DB_URL'),
                   mongo_db=crawler.settings.get('DB_NAME'))

    def open_spider(self, spider):
        self.client = MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        print(f'\nProcessing climate item: \n{item}\n')
        self.db[self.collection_name].insert_one(ItemAdapter(item).asdict())
        return item
