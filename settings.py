#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/11/20 4:20 PM
@Author: liang
@File: settings.py
"""
import os

PROJECT_BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SPIDER_DIRNAME = 'gaivota_spiders'
SPIDER_PROJECT_PATH = os.path.join(PROJECT_BASE_DIR, SPIDER_DIRNAME)
SCRAPY_SETTINGS_MODULE = 'gaivota_spiders.settings'


